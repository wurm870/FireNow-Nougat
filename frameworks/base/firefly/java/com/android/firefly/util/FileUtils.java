package android.firefly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class FileUtils {

	public  static String readFromFile(File file) {
		if ((file != null) && file.exists()) {
			try {
				FileInputStream fin = new FileInputStream(file);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(fin));
				String value = reader.readLine();
				fin.close();
				return value;
			} catch (IOException e) {
				return null;
			}
		}
		return null;
	}

	public  static boolean write2File(File file, String value) {
		if ((file == null) || (!file.exists()))
			return false;
		try {
			FileOutputStream fout = new FileOutputStream(file);
			PrintWriter pWriter = new PrintWriter(fout);
			pWriter.println(value);
			pWriter.flush();
			pWriter.close();
			fout.close();
			return true;
		} catch (IOException re) {
			return false;
		}
	}
}