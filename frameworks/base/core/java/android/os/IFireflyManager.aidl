package android.os;

interface IFireflyManager {
    void reboot(boolean confirm,String reason);
    void shutdown(boolean confirm);
    void sleep();

    void takeScreenshot(String path,String name);

    void setRotation (int  rotation);
    int getRotation ();
    void thawRotation();
    void setStatusBar(boolean show);
}